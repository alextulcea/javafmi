/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmu.tester.command;

import org.fmu.tester.Command;
import org.fmu.tester.Simulation;
import org.fmu.tester.VariableLoggerList;

public class CommandHelp extends Command {

    public CommandHelp(VariableLoggerList variableLoggerList) {
        super(variableLoggerList);
    }

    @Override
    public String execute(Simulation simulation) {
        StringBuilder builder = new StringBuilder();
        builder.append("Available commands:\n");
        builder.append(CommandGet.getUsage()).append("\n");
        builder.append(CommandSet.getUsage()).append("\n");
        builder.append(CommandLog.getUsage()).append("\n");
        builder.append(CommandInit.getUsage()).append("\n");
        builder.append(CommandStep.getUsage()).append("\n");
        builder.append(CommandMultiStep.getUsage()).append("\n");
        builder.append(CommandReset.getUsage()).append("\n");
        builder.append(CommandTerminate.getUsage()).append("\n");
        builder.append(CommandHelp.getUsage()).append("\n");
        return builder.toString();
    }

    public static String getUsage() {
        return "help";
    }
}
