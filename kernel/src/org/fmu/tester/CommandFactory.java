/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmu.tester;

import org.fmu.tester.command.CommandGet;
import org.fmu.tester.command.CommandHelp;
import org.fmu.tester.command.CommandInit;
import org.fmu.tester.command.CommandLog;
import org.fmu.tester.command.CommandMultiStep;
import org.fmu.tester.command.CommandReset;
import org.fmu.tester.command.CommandSet;
import org.fmu.tester.command.CommandStep;
import org.fmu.tester.command.CommandTerminate;

public class CommandFactory {

    public static Command createCommand(String in, VariableLoggerList variableLoggerList) {
        if (in.startsWith(Command.GET))
            return buildGetCommand(in, variableLoggerList);
        else if (in.startsWith(Command.SET))
            return buildSetCommand(in, variableLoggerList);
        else if (in.startsWith(Command.LOG))
            return buildLogCommand(in, variableLoggerList);
        else if (in.startsWith(Command.INIT))
            return buildInitCommand(in, variableLoggerList);
        else if (in.startsWith(Command.STEP))
            return buildStepCommand(in, variableLoggerList);
        else if (in.startsWith(Command.MULTI_STEP))
            return buildMultiStepCommand(in, variableLoggerList);
        else if (in.startsWith(Command.RESET))
            return buildResetCommand(in, variableLoggerList);
        else if (in.startsWith(Command.TERMINATE))
            return buildTerminateCommand(in, variableLoggerList);
        return new CommandHelp(variableLoggerList);
    }

    private static Command buildGetCommand(String in, VariableLoggerList variableLoggerList) {
        String[] command = in.split(" ");
        if (command.length != 2)
            return new CommandHelp(variableLoggerList);
        return new CommandGet(command[1], variableLoggerList);
    }

    private static Command buildSetCommand(String in, VariableLoggerList variableLoggerList) {
        String[] command = in.split(" ");
        if (command.length != 3)
            return new CommandHelp(variableLoggerList);
        return new CommandSet(command[1], command[2], variableLoggerList);
    }

    private static Command buildLogCommand(String in, VariableLoggerList variableLoggerList) {
        String[] command = in.split(" ");
        if (command.length != 2)
            return new CommandHelp(variableLoggerList);
        return new CommandLog(command[1], variableLoggerList);
    }

    private static Command buildInitCommand(String in, VariableLoggerList variableLoggerList) {
        return new CommandInit(variableLoggerList);
    }

    private static Command buildStepCommand(String in, VariableLoggerList variableLoggerList) {
        String[] command = in.split(" ");
        if (command.length != 2)
            return new CommandHelp(variableLoggerList);
        return new CommandStep(command[1], variableLoggerList);
    }

    private static Command buildMultiStepCommand(String in, VariableLoggerList variableLoggerList) {
        String[] command = in.split(" ");
        if (command.length != 3)
            return new CommandHelp(variableLoggerList);
        return new CommandMultiStep(command[1], command[2], variableLoggerList);
    }

    private static Command buildResetCommand(String in, VariableLoggerList variableLoggerList) {
        return new CommandReset(variableLoggerList);
    }

    private static Command buildTerminateCommand(String in, VariableLoggerList variableLoggerList) {
        return new CommandTerminate(variableLoggerList);
    }
}
