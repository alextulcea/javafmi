/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.fmutest.presentation.web.library;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.fmutest.presentation.web.utils.StreamHelper;

public class LibraryFile {

    public static String getBasename(String sFilename) {
        Integer iPos = sFilename.lastIndexOf(".");
        if (iPos == -1) return null;
        return sFilename.substring(0, iPos);
    }

    public static String getDirname(String sFilename) {
        sFilename = LibraryString.replaceAll(sFilename, "\\", "/");
        Integer iPos = sFilename.lastIndexOf("/");
        if (iPos == -1) return null;
        return sFilename.substring(0, iPos);
    }

    public static String getFilename(String sFilename) {
        sFilename = sFilename.replaceAll("\\\\", "/");
        Integer iPos = sFilename.lastIndexOf("/");
        if (iPos == -1) return sFilename;
        return sFilename.substring(iPos + 1);
    }

    public static String getFilenameWithoutExtension(String sFilename) {
        sFilename = sFilename.replaceAll("\\\\", "/");
        Integer iPos = sFilename.lastIndexOf("/");
        int iDotPos = sFilename.lastIndexOf(".");
        if (iPos == -1) return null;
        return sFilename.substring(iPos + 1, iDotPos);
    }

    public static String getExtension(String sFilename) {
        Integer iPos = sFilename.lastIndexOf(".");
        if (iPos == -1) return null;
        return sFilename.substring(iPos + 1);
    }

    public static Boolean writeFile(File file, InputStream content) {
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            copyStream(content, outputStream);

        } catch (IOException oException) {
            throw new RuntimeException("Could not write file: " + file.getName(), oException);
        } finally {
            StreamHelper.close(outputStream);
        }

        return true;
    }

    public static void copyStream(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[4096];
        int readed = 0;
        while ((readed = input.read(buffer)) > 0)
            output.write(buffer, 0, readed);
    }

    public static void deleteAll(File workingDirectory) {
        for (File file : workingDirectory.listFiles())
            if (file.isDirectory()) deleteAll(file);
            else file.delete();
        workingDirectory.delete();
    }
}