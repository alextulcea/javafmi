/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package javafmi.operatingsystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafmi.Common;
import org.javafmi.operatingsystem.PathBuilder;
import org.javafmi.operatingsystem.Unzipper;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Test;

public class UnzipperTest {

    private static String outPath = "tmp";
    private String modelDescription = "modelDescription.xml";
    private String sourcePath = PathBuilder.buildPath(new String[]{"resources", "fmu", "cosimulation"});

    @AfterClass
    public static void clean() {
        Common.deleteAll(outPath);
    }

    @Test
    public void testUnzipModelDescription() {
        File fmuFile = getFmuFile("bouncingBall.fmu");
        Unzipper unzipper = new Unzipper(fmuFile);
        File modelDescriptionFile = unzipper.unzip(modelDescription, outPath);
        assertEquals("08441861ad7298c191209b764f15f2d2", calculateChecksum(modelDescriptionFile));
        assertEquals(PathBuilder.buildPath(outPath, modelDescription), modelDescriptionFile.getPath());
    }

    @Test
    public void testUnzipWin32LibraryFile() {
        File fmuFile = getFmuFile("bouncingBall.fmu");
        Unzipper unzipper = new Unzipper(fmuFile);
        File libraryFile = unzipper.unzip("binaries/win32/bouncingBall.dll", outPath);
        assertEquals("5269558d1cd1aa76e1be31f3cc7875a4", calculateChecksum(libraryFile));
        assertEquals(PathBuilder.buildPath(outPath, "bouncingBall.dll"), libraryFile.getPath());
    }

    @Test
    public void testUnzipSFRV1CSLibrary() {
        File fmuFile = getFmuFile("SFRV1CS.fmu");
        Unzipper unzipper = new Unzipper(fmuFile);
        File library = unzipper.unzip("binaries/win32/SFR.dll", outPath);
        assertEquals("938EFB2FB542A16ECC49098A5CCAFFCE".toLowerCase(), calculateChecksum(library));
        assertEquals(PathBuilder.buildPath(outPath, "SFR.dll"), library.getPath());
    }

    private File getFmuFile(String fmuFile) {
        return new File(PathBuilder.buildPath(sourcePath, fmuFile));
    }

    private String calculateChecksum(File file) {
        FileInputStream inputStream = null;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            inputStream = new FileInputStream(file);
            byte[] dataBytes = new byte[1024];
            int bytesRead;
            while (true) {
                if ((bytesRead = inputStream.read(dataBytes)) < 0)
                    break;
                messageDigest.update(dataBytes, 0, bytesRead);
            }

            byte[] digestBytes = messageDigest.digest();
            StringBuilder stringBuffer = new StringBuilder("");
            for (int i = 0; i < digestBytes.length; i++)
                stringBuffer.append(Integer.toString((digestBytes[i] & 0xff) + 0x100, 16).substring(1));
            inputStream.close();
            return stringBuffer.toString();


        } catch (IOException | NoSuchAlgorithmException ex) {
            Logger.getLogger(UnzipperTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                inputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(UnzipperTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
}
