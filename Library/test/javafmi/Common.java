/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package javafmi;

import java.io.File;
import org.javafmi.FmuLocation;
import org.javafmi.Status;
import org.javafmi.operatingsystem.PathBuilder;
import org.javafmi.wrapper.FmuProxy;

public class Common {

    public static FmuProxy getProxyForBouncingBall() {
        return new FmuProxy(getFmuLocationFromName("bouncingBall.fmu"));
    }

    public static FmuProxy getProxyForIncrements() {
        return new FmuProxy(getFmuLocationFromName("inc.fmu"));
    }

    public static FmuProxy getProxyForValues() {
        return new FmuProxy(getFmuLocationFromName("values.fmu"));
    }

    public static FmuProxy getProxyForGridSys() {
        return new FmuProxy(getFmuLocationFromName("GridSysPro_0V1_Examples_Validation_0with_0generator.fmu"));
    }

    public static Status initializeProxy(FmuProxy proxy) {
        double startTime = 0;
        double stopTime = 1;
        return proxy.initializeSlave(startTime, stopTime);
    }

    public static FmuLocation getFmuLocationFromName(String fmuName) {
        return new FmuLocation<>(PathBuilder.buildPath(new String[]{"resources", "fmu", "cosimulation"}, fmuName));
    }

    public static void instantiateProxy(FmuProxy proxy) {
        double timeout = 100000;
        proxy.instantiateSlave(timeout);
    }

    public static void deleteAll(String outputPath) {
        File out = new File(outputPath);
        for (File file : out.listFiles())
            if (file.isDirectory())
                deleteAll(file.getPath());
            else
                file.delete();
        out.delete();
    }
}
