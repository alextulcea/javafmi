/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package javafmi.fmuproxy;

import static javafmi.Common.*;
import org.javafmi.Status;
import org.javafmi.wrapper.FmuProxy;
import org.junit.Assert;
import org.junit.Test;

public class FmuProxyComputationTest {

    @Test
    public void testDoStepInBouncingBall() {
        FmuProxy proxy = getProxyForBouncingBall();
        instantiateProxy(proxy);
        initializeProxy(proxy);
        double[] height = {1.0, 0.955855, 0.813609, 0.573264, 0.234819, 0.105131};
        double stepSize = 0.1;
        for (int i = 0; i < height.length; i++) {
            Assert.assertEquals(height[i], proxy.getReal(0), 0.001);
            proxy.doStep(stepSize);
        }
        proxy.terminateSlave();
        proxy.freeSlave();
    }

    @Test
    public void testDoStepInIncrements() {
        FmuProxy proxy = getProxyForIncrements();
        instantiateProxy(proxy);
        initializeProxy(proxy);
        for (int i = 0; i < 10; i++)
            Assert.assertEquals(Status.OK, proxy.doStep(1));
        proxy.terminateSlave();
        proxy.freeSlave();
    }

    @Test
    public void testStepCantBeCancelled() {
        FmuProxy proxy = getProxyForBouncingBall();
        instantiateProxy(proxy);
        initializeProxy(proxy);
        double stepSize = 0.1;
        Assert.assertEquals(Status.OK, proxy.doStep(stepSize));
        Assert.assertEquals(Status.ERROR, proxy.cancelStep());
        Assert.assertEquals(Status.OK, proxy.doStep(stepSize));
        Assert.assertEquals(Status.OK, proxy.terminateSlave());
        proxy.freeSlave();
    }
}
