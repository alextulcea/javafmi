/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package javafmi.fmuproxy;

import static javafmi.Common.*;
import org.javafmi.Status;
import org.javafmi.modeldescription.modelvariables.ScalarVariable;
import org.javafmi.wrapper.FmuProxy;
import org.junit.Assert;
import org.junit.Test;

public class FmuProxyTransferOfInputOutputTest {

    @Test
    public void testGetReal() {
        FmuProxy proxy = getProxyForBouncingBall();
        instantiateProxy(proxy);
        initializeProxy(proxy);
        Assert.assertEquals(1.0, proxy.getReal(0), 0.01);
        Assert.assertEquals(0.0, proxy.getReal(2), 0.01);
        Assert.assertEquals(-9.81, proxy.getReal(3), 0.01);
        Assert.assertEquals(0.7, proxy.getReal(4), 0.01);
        proxy.terminateSlave();
        proxy.freeSlave();
    }

    @Test
    public void testGetInteger() {
        FmuProxy proxy = getProxyForIncrements();
        instantiateProxy(proxy);
        initializeProxy(proxy);
        Assert.assertEquals(1.0, proxy.getInteger(0), 0.01);
        proxy.terminateSlave();
        proxy.freeSlave();
    }

    @Test
    public void testGetBoolean() {
        FmuProxy proxy = getProxyForValues();
        instantiateProxy(proxy);
        initializeProxy(proxy);
        Assert.assertTrue(proxy.getBoolean(0));
        proxy.terminateSlave();
        proxy.freeSlave();
    }

    @Test
    public void testGetString() {
        FmuProxy proxy = getProxyForValues();
        instantiateProxy(proxy);
        initializeProxy(proxy);
        Assert.assertEquals("jan", proxy.getString(1));
        proxy.terminateSlave();
        proxy.freeSlave();
    }

    @Test
    public void testGetEnumeration() {
        FmuProxy proxy = getProxyForGridSys();
        instantiateProxy(proxy);
        initializeProxy(proxy);
        Assert.assertEquals("InitialOutput", proxy.getEnumeration(getVariable(proxy, "regulation_vitesse.transferFunction.initType")));
        proxy.terminateSlave();
        proxy.freeSlave();
    }

    @Test
    public void testSetReal() {
        FmuProxy proxy = getProxyForBouncingBall();
        instantiateProxy(proxy);
        Assert.assertEquals(Status.OK, proxy.setReal(0, 50.0));
        initializeProxy(proxy);
        Assert.assertEquals(50, proxy.getReal(0), 0.01);
        proxy.terminateSlave();
        proxy.freeSlave();
    }

    @Test
    public void testSetInteger() {
        FmuProxy proxy = getProxyForValues();
        instantiateProxy(proxy);
        Assert.assertEquals(Status.OK, proxy.setInteger(0, 5));
        initializeProxy(proxy);
        Assert.assertEquals(5, proxy.getInteger(0), 0.01);
        proxy.terminateSlave();
        proxy.freeSlave();
    }

    private ScalarVariable getVariable(FmuProxy proxy, String variableName) {
        return proxy.getModelDescription().getScalarVariable(variableName);
    }
}
