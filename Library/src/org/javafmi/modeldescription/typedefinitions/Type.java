/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.javafmi.modeldescription.typedefinitions;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class Type {

    @Attribute(required = false)
    private String name;
    @Attribute(required = false)
    private String description;
    @Attribute(required = false)
    private String quantity;
    @Attribute(required = false)
    private double min;
    @Attribute(required = false)
    private double max;
    @Attribute(required = false)
    private String declaredType;
    @Attribute(required = false)
    private boolean fixed;
    @Element(name = "RealType", required = false)
    private RealType realType;
    @Element(name = "BooleanType", required = false)
    private BooleanType booleanType;
    @Element(name = "IntegerType", required = false)
    private IntegerType integerType;
    @Element(name = "StringType", required = false)
    private StringType stringType;
    @Element(name = "EnumerationType", required = false)
    private EnumerationType enumerationType;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getQuantity() {
        return quantity;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    public String getDeclaredType() {
        return declaredType;
    }

    public boolean isFixed() {
        return fixed;
    }

    public Type getType() {
        if (realType != null)
            return realType;
        if (integerType != null)
            return integerType;
        if (booleanType != null)
            return booleanType;
        if (stringType != null)
            return stringType;
        if (enumerationType != null)
            return enumerationType;
        return null;
    }

    public Object getStart() {
        return null;
    }
}
