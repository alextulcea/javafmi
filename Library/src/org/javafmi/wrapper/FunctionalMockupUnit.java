/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.javafmi.wrapper;

import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import java.util.ArrayList;
import java.util.List;

public interface FunctionalMockupUnit extends FunctionalMockupInterface {

    public static List<Pointer> onMemoryAllocatedObjects = new ArrayList<>();

    public class Logger implements CallbackLogger {

        @Override
        public void record(Pointer fmiComponent, String logSource, int status, String category, String message, Pointer parameters) {
            System.out.print(logSource + " reports: " + category);
            System.out.format(" with status %d \n", status);
            System.out.format(message.replace("%u", "%d") + "\n", parameters.getString(0), parameters.getInt(4));
        }
    }

    public class AllocateMemory implements CallbackAllocateMemory {

        private int MEMORY_OFFSET = 4;

        @Override
        public Pointer allocate(Size_T numberOfObjects, Size_T objectSize) {
            Memory memoryToAllocate = new Memory(fixNumerOfObjects(numberOfObjects.intValue()) * objectSize.intValue());
            memoryToAllocate.align(MEMORY_OFFSET);
            memoryToAllocate.clear();
            Pointer allocatedObject = memoryToAllocate.share(0);
            onMemoryAllocatedObjects.add(allocatedObject);
            return allocatedObject;
        }

        private int fixNumerOfObjects(int numberOfObjects) {
            return (numberOfObjects <= 0) ? 1 : numberOfObjects;
        }
    }

    public class FreeMemory implements CallbackFreeMemory {

        @Override
        public void free(Pointer object) {
            onMemoryAllocatedObjects.remove(object);
        }
    }

    public class StepFinished implements CallbackStepFinished {

        @Override
        public void apply(Pointer fmiComponent, int status) {
        }
    }
}
