/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.javafmi.wrapper;

import com.sun.jna.Function;
import com.sun.jna.Memory;
import com.sun.jna.NativeLibrary;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.javafmi.FmuLocation;
import org.javafmi.ModelDescription;
import org.javafmi.Status;
import static org.javafmi.Status.*;
import org.javafmi.modeldescription.modelvariables.ScalarVariable;
import org.javafmi.modeldescription.typedefinitions.EnumerationType;
import org.javafmi.operatingsystem.FmuFileExtractor;
import static org.javafmi.wrapper.FmuFunctionSuffix.*;
import org.javafmi.wrapper.datatypes.NativeBoolean;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

public class FmuProxy {
    //TODO si se hace un get de una variable que ya estaba consultada en el mismo step, no hacer la solicitud a la FMU
    //TODO si hago un set de una variable que no ha cambiado respecto del ultimo valor, el set no se hace
    //TODO hacer los set solo antes del dostep
    //TODO si se hacen varios set en un mismo step de una misma variable, solo transmitir la ultima en caso contrario caso 2
    //TODO Los get bajo demanda

    //TODO prueba instanciar muchas FMUs
    private static final String MYME_TYPE = "application/x-fmu-sharedlibrary";
    private final String fmuLocation;
    private final String proxyID;
    private final String modelIdentifier;
    private final ModelDescription modelDescription;
    private final String libraryPath;
    private NativeLibrary libraryInstance;
    private boolean isInstanceVisible;
    private boolean isInstanceInteractive;
    private boolean isDoingLogging;
    private boolean isStopTimeDefined;
    private double startTime;
    private double stopTime;
    private double currentSimulationTime;
    private double timeout;
    private Pointer instantiatedSlave;

    public FmuProxy(FmuLocation location) {
        this.fmuLocation = location.toString();
        FmuFileExtractor fileExtractor = new FmuFileExtractor(new File(fmuLocation));
        this.modelDescription = deserializeModelDescriptionFile(fileExtractor.getModelDescriptionFile());
        this.libraryPath = fileExtractor.getLibraryPath(modelDescription.getModelIdentifier());
        this.proxyID = UUID.randomUUID().toString();
        this.modelIdentifier = getModelIdentifier(modelDescription);
        this.isInstanceVisible = false;
        this.isInstanceInteractive = false;
        this.isDoingLogging = false;
        this.isStopTimeDefined = true;
    }

    private ModelDescription deserializeModelDescriptionFile(File modelDescriptionFile) {
        try {
            Serializer serializer = new Persister();
            return serializer.read(ModelDescription.class, modelDescriptionFile);
        } catch (Exception ex) {
            Logger.getLogger(FmuProxy.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private String getModelIdentifier(ModelDescription modelDescription) {
        return modelDescription.getModelIdentifier();
    }

    public void instantiateSlave(double timeout) {
        this.timeout = timeout;
        this.libraryInstance = NativeLibrary.getInstance(libraryPath);
        this.instantiatedSlave = (Pointer) getFunction(INSTANTIATE).invoke(Pointer.class, getParametersForInstantiate());
    }

    public Status initializeSlave(double startTime, double stopTime) {
        this.currentSimulationTime = startTime;
        this.startTime = startTime;
        this.stopTime = stopTime;
        return getStatus((Integer) getFunction(INITIALIZE).invoke(Integer.class, getParametersForInitialize()));
    }

    public Status terminateSlave() {
        return getStatus((Integer) getFunction(TERMINATE).invoke(Integer.class, new Object[]{instantiatedSlave}));
    }

    public Status resetSlave() {
        return getStatus((Integer) getFunction(RESET).invoke(Integer.class, new Object[]{instantiatedSlave}));
    }

    public void freeSlave() {
        getFunction(FREE).invoke(new Object[]{instantiatedSlave});
        libraryInstance.dispose();
    }

    public Status doStep(double stepSize) {
        Status doStepStatus = getStatus((Integer) getFunction(DO_STEP).invoke(Integer.class, getParametersForDoStep(stepSize)));
        if (doStepStatus.equals(Status.OK))
            currentSimulationTime += stepSize;
        return doStepStatus;
    }

    public Status cancelStep() {
        return getStatus((Integer) getFunction(CANCEL_STEP).invoke(Integer.class, new Object[]{instantiatedSlave}));
    }

    public Object getEnumeration(ScalarVariable modelVariable) {
        EnumerationType enumeration = (EnumerationType) modelVariable.getType();
        Integer enumerationIndex;
        EnumerationType enumarationType = (EnumerationType) modelDescription.getTypeFromTypeDefinition(enumeration.getDeclaredType());
        if (modelVariable.getVariability().equalsIgnoreCase("constant")) {
            enumerationIndex = enumeration.getStart();
            return enumarationType.getItems().get(enumerationIndex - 1).getName();
        }
        enumerationIndex = getInteger(modelVariable.getValueReference());
        return enumarationType.getItems().get(enumerationIndex - 1).getName();

    }

    public Double getReal(Integer valueReference) {
        DoubleBuffer outBuffer = DoubleBuffer.allocate(1);
        getFunction(GET_REAL).invoke(getParametersForFmuGetterSetter(valueReference, outBuffer));
        return outBuffer.get(0);
    }

    public int getInteger(Integer valueReference) {
        IntBuffer outBuffer = IntBuffer.allocate(1);
        getFunction(GET_INTEGER).invoke(getParametersForFmuGetterSetter(valueReference, outBuffer));
        return outBuffer.get(0);
    }

    public boolean getBoolean(Integer valueReference) {
        ByteBuffer outBuffer = ByteBuffer.allocate(1);
        getFunction(GET_BOOLEAN).invoke(getParametersForFmuGetterSetter(valueReference, outBuffer));
        return NativeBoolean.getBooleanFor(outBuffer.get(0));
    }

    public String getString(Integer valueReference) {
        PointerByReference outPointerByReference = new PointerByReference();
        getFunction(GET_STRING).invoke(getParametersForFmuGetterSetter(valueReference, outPointerByReference));
        Pointer stringPointer = outPointerByReference.getValue();
        return (stringPointer != null) ? stringPointer.getString(0) : "";
    }

    public Status setReal(Integer valueReference, Double newValue) {
        DoubleBuffer valueBuffer = DoubleBuffer.allocate(1).put(0, newValue);
        return getStatus((Integer) getFunction(SET_REAL).invoke(Integer.class, getParametersForFmuGetterSetter(valueReference, valueBuffer)));
    }

    public Status setInteger(Integer valueReference, Integer newValue) {
        IntBuffer valueBuffer = IntBuffer.allocate(1).put(0, newValue);
        return getStatus((Integer) getFunction(SET_INTEGER).invoke(Integer.class, getParametersForFmuGetterSetter(valueReference, valueBuffer)));
    }

    public Status setBoolean(Integer valueReference, Boolean newValue) {
        ByteBuffer valueBuffer = ByteBuffer.allocate(1).put(0, NativeBoolean.getValueFor(newValue));
        return getStatus((Integer) getFunction(SET_INTEGER).invoke(Integer.class, getParametersForFmuGetterSetter(valueReference, valueBuffer)));
    }

    public Status setString(Integer valueReference, String newValue) {
        PointerByReference pointerByReference = new PointerByReference();
        Pointer reference = new Memory(newValue.length() + 1).share(0);
        reference.setString(0, newValue);
        pointerByReference.setValue(reference);
        return getStatus((Integer) getFunction(SET_INTEGER).invoke(Integer.class, getParametersForFmuGetterSetter(valueReference, pointerByReference)));
    }

    public ModelDescription getModelDescription() {
        return modelDescription;
    }

    public boolean isInstanceVisible() {
        return isInstanceVisible;
    }

    public void setIsInstanceVisible(boolean isInstanceVisible) {
        this.isInstanceVisible = isInstanceVisible;
    }

    public boolean isInstanceInteractive() {
        return isInstanceInteractive;
    }

    public void setIsInstanceInteractive(boolean isInstanceInteractive) {
        this.isInstanceInteractive = isInstanceInteractive;
    }

    public boolean isDoingLogging() {
        return isDoingLogging;
    }

    public Status setIsDoingLogging(boolean isDoingLogging) {
        this.isDoingLogging = isDoingLogging;
        return getStatus((Integer) getFunction(SET_DEBUG).invoke(Integer.class, getParametersForSetDebugLogging()));
    }

    private Function getFunction(String suffix) {
        return libraryInstance.getFunction(modelIdentifier + suffix);
    }

    private Object[] getParametersForInstantiate() {
        return new Object[]{proxyID, modelDescription.getGuid(), fmuLocation, MYME_TYPE, timeout, NativeBoolean.getValueFor(isInstanceVisible), NativeBoolean.getValueFor(isInstanceInteractive), new CallbackFunctions.ByValue(), NativeBoolean.getValueFor(isDoingLogging)};
    }

    private Object[] getParametersForInitialize() {
        return new Object[]{instantiatedSlave, startTime, NativeBoolean.getValueFor(isStopTimeDefined), stopTime};
    }

    private Object[] getParametersForDoStep(double stepSize) {
        return new Object[]{instantiatedSlave, currentSimulationTime, stepSize, NativeBoolean.getValueFor(true)};
    }

    private Object[] getParametersForFmuGetterSetter(Integer valueReference, Object outBuffer) {
        return new Object[]{instantiatedSlave, IntBuffer.allocate(1).put(0, valueReference), new Size_T(1), outBuffer};
    }

    private Object[] getParametersForSetDebugLogging() {
        return new Object[]{instantiatedSlave, NativeBoolean.getValueFor(isDoingLogging)};
    }

    public double getCurrentSimulationTime() {
        return currentSimulationTime;
    }
}
