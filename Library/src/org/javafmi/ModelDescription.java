/*

 Copyright 2013, SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */
package org.javafmi;

import java.util.List;
import org.javafmi.modeldescription.DefaultExperiment;
import org.javafmi.modeldescription.UnitDefinitions.BaseUnit;
import org.javafmi.modeldescription.modelvariables.ScalarVariable;
import org.javafmi.modeldescription.typedefinitions.Type;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "fmiModelDescription", strict = false)
public class ModelDescription {

    @Attribute
    private String fmiVersion;
    @Attribute
    private String modelName;
    @Attribute
    private String modelIdentifier;
    @Attribute
    private String guid;
    @Attribute(required = false)
    private String description;
    @Attribute(required = false)
    private String author;
    @Attribute(required = false)
    private String version;
    @Attribute(required = false)
    private String generationTool;
    @Attribute(required = false)
    private String generationDateAndTime;
    @Attribute(required = false)
    private String variableNamingConvention;
    @Attribute
    private String numberOfContinuousStates;
    @Attribute
    private String numberOfEventIndicators;
    @ElementList(name = "UnitDefinitions", required = false)
    private List<BaseUnit> unitDefinitions;
    @ElementList(name = "TypeDefinitions", required = false)
    private List<Type> typeDefinitions;
    @Element(name = "DefaultExperiment", required = false)
    private DefaultExperiment defaultExperiment;
    @ElementList(name = "ModelVariables", required = false)
    private List<ScalarVariable> modelVariables;

    public String getFmiVersion() {
        return fmiVersion;
    }

    public String getModelName() {
        return modelName;
    }

    public String getModelIdentifier() {
        return modelIdentifier;
    }

    public String getGuid() {
        return guid;
    }

    public String getDescription() {
        return description;
    }

    public String getAuthor() {
        return author;
    }

    public String getVersion() {
        return version;
    }

    public String getGenerationTool() {
        return generationTool;
    }

    public String getGenerationDateAndTime() {
        return generationDateAndTime;
    }

    public String getVariableNamingConvention() {
        return variableNamingConvention;
    }

    public String getNumberOfContinuousStates() {
        return numberOfContinuousStates;
    }

    public String getNumberOfEventIndicators() {
        return numberOfEventIndicators;
    }

    public List<BaseUnit> getUnitDefinitions() {
        return unitDefinitions;
    }

    public List<Type> getTypeDefinitions() {
        return typeDefinitions;
    }

    public Type getTypeFromTypeDefinition(String name) {
        for (Type type : typeDefinitions)
            if (type.getName().equals(name)) return type.getType();
        return null;
    }

    public DefaultExperiment getDefaultExperiment() {
        return defaultExperiment;
    }

    public List<ScalarVariable> getModelVariables() {
        return modelVariables;
    }

    public ScalarVariable getScalarVariable(String name) {
        for (ScalarVariable scalarVariable : modelVariables)
            if (scalarVariable.getName().equals(name))
                return scalarVariable;
        return null;
    }
}
