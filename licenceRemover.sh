#!/bin/bash
# this script removes all from the first line until the package line - 1
# do not put relevant information in this place


function firstLineOf {
    local pattern=$1
    local file=$2
    echo $(grep -En "$pattern" $file | awk -F: '{print $1}')
}

function removeLinesUntil {
    local lastLine=$1
    local file=$2
    sed -i -e "1,${lastLine}d" $file
}

srcPath=$1
for file in $(find $srcPath -name "*.java")
do
    packagePreviousLine=$(firstLineOf "package .*;" $file)
    packagePreviousLine=$(( $packagePreviousLine - 1 ))
    if [[ $packagePreviousLine -gt 1 ]]
    then
        removeLinesUntil $packagePreviousLine $file
    fi
done
